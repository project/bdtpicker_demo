#!/bin/sh

self_path=`dirname $0`

profile=`ls -1 ${self_path}/*.profile | head -1`
profile=${profile##*/}
profile=${profile%.*}

drush make --no-recursion --contrib-destination="profiles/${profile}" "${self_path}/simplytest.make"
