
; Modules.
projects[admin_menu][version] = "3.0-rc4"

projects[bdtpicker][version] = "3.x-dev"

projects[ctools][version] = "1.4"
projects[ctools][patch][2195211] = "http://www.drupal.org/files/issues/ctools-combined_patch_from_n2195211-15_and_n2195471-29--n2195471-47.patch"

projects[date][version] = "2.8"
projects[date][patch][2359653] = "https://www.drupal.org/files/issues/date-ampm-parse-2359653-01.patch"

projects[features][version] = "2.2"
projects[features][patch][2362203] = "https://www.drupal.org/files/issues/features-language-weight-2362203-01.patch"

; Revision points to v2.4 + 5 commits.
projects[jquery_update][download][type] = "git"
projects[jquery_update][download][branch] = "7.x-2.x"
projects[jquery_update][download][revision] = "1ad9a08"

projects[l10n_update][version] = "2.0-rc2"

projects[libraries][version] = "2.2"

projects[moment][version] = "2.x-dev"

projects[strongarm][version] = "2.0"

; Themes.
projects[bootstrap][download][type] = "git"
projects[bootstrap][download][branch] = "7.x-3.x"
projects[bootstrap][download][revision] = "e014953"
projects[bootstrap][patch][2224211] = "http://www.drupal.org/files/issues/2224211-collasible-fieldset-remove-class.patch"
