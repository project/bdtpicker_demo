
api = 2
core = 7.x

defaults[projects][subdir] = "contrib"

includes[] = project-contrib.make
includes[] = project-libraries-verified.make
includes[] = project-libraries-other.make

translations[] = de
translations[] = es
translations[] = fr
