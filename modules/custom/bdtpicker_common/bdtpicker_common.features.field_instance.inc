<?php
/**
 * @file
 * bdtpicker_common.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bdtpicker_common_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-bdtpicker_tester-bdtpicker_mn'
  $field_instances['node-bdtpicker_tester-bdtpicker_mn'] = array(
    'bundle' => 'bdtpicker_tester',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'bdtpicker_mn',
    'label' => 'Single',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'bdtpicker_activate_value' => 1,
        'bdtpicker_activate_value2' => 1,
        'bdtpicker_chain_next' => 1,
        'bdtpicker_chain_previous' => 0,
        'bdtpicker_days_of_week_disabled' => array(
          'friday' => 0,
          'monday' => 0,
          'saturday' => 0,
          'sunday' => 0,
          'thursday' => 0,
          'tuesday' => 0,
          'wednesday' => 0,
        ),
        'bdtpicker_enabled' => 1,
        'bdtpicker_side_by_side' => 0,
        'increment' => 10,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-bdtpicker_tester-bdtpicker_mo'
  $field_instances['node-bdtpicker_tester-bdtpicker_mo'] = array(
    'bundle' => 'bdtpicker_tester',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'bdtpicker_mo',
    'label' => 'End date is optional',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'bdtpicker_activate_value' => 1,
        'bdtpicker_activate_value2' => 1,
        'bdtpicker_chain_next' => 1,
        'bdtpicker_chain_previous' => 0,
        'bdtpicker_days_of_week_disabled' => array(
          'friday' => 0,
          'monday' => 0,
          'saturday' => 0,
          'sunday' => 0,
          'thursday' => 0,
          'tuesday' => 0,
          'wednesday' => 0,
        ),
        'bdtpicker_enabled' => 1,
        'bdtpicker_side_by_side' => 0,
        'increment' => 20,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-bdtpicker_tester-bdtpicker_mr'
  $field_instances['node-bdtpicker_tester-bdtpicker_mr'] = array(
    'bundle' => 'bdtpicker_tester',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'bdtpicker_mr',
    'label' => 'End date is required',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'bdtpicker_activate_value' => 1,
        'bdtpicker_activate_value2' => 1,
        'bdtpicker_chain_next' => 1,
        'bdtpicker_chain_previous' => 0,
        'bdtpicker_days_of_week_disabled' => array(
          'friday' => 0,
          'monday' => 0,
          'saturday' => 'saturday',
          'sunday' => 'sunday',
          'thursday' => 0,
          'tuesday' => 0,
          'wednesday' => 0,
        ),
        'bdtpicker_enabled' => 1,
        'bdtpicker_side_by_side' => 1,
        'increment' => 60,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-bdtpicker_tester-bdtpicker_sn'
  $field_instances['node-bdtpicker_tester-bdtpicker_sn'] = array(
    'bundle' => 'bdtpicker_tester',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'bdtpicker_sn',
    'label' => 'Single',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'bdtpicker_activate_value' => 1,
        'bdtpicker_activate_value2' => 1,
        'bdtpicker_chain_next' => 1,
        'bdtpicker_chain_previous' => 0,
        'bdtpicker_days_of_week_disabled' => array(
          'friday' => 0,
          'monday' => 0,
          'saturday' => 0,
          'sunday' => 0,
          'thursday' => 0,
          'tuesday' => 0,
          'wednesday' => 0,
        ),
        'bdtpicker_enabled' => 1,
        'bdtpicker_side_by_side' => 0,
        'increment' => 1,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-bdtpicker_tester-bdtpicker_so'
  $field_instances['node-bdtpicker_tester-bdtpicker_so'] = array(
    'bundle' => 'bdtpicker_tester',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'bdtpicker_so',
    'label' => 'End date is optional',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'bdtpicker_activate_value' => 1,
        'bdtpicker_activate_value2' => 1,
        'bdtpicker_chain_next' => 1,
        'bdtpicker_chain_previous' => 0,
        'bdtpicker_days_of_week_disabled' => array(
          'friday' => 0,
          'monday' => 0,
          'saturday' => 0,
          'sunday' => 0,
          'thursday' => 0,
          'tuesday' => 0,
          'wednesday' => 0,
        ),
        'bdtpicker_enabled' => 1,
        'bdtpicker_side_by_side' => 0,
        'increment' => 2,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-bdtpicker_tester-bdtpicker_sr'
  $field_instances['node-bdtpicker_tester-bdtpicker_sr'] = array(
    'bundle' => 'bdtpicker_tester',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'bdtpicker_sr',
    'label' => 'End date is required',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'bdtpicker_activate_value' => 1,
        'bdtpicker_activate_value2' => 1,
        'bdtpicker_chain_next' => 1,
        'bdtpicker_chain_previous' => 0,
        'bdtpicker_days_of_week_disabled' => array(
          'friday' => 0,
          'monday' => 0,
          'saturday' => 0,
          'sunday' => 0,
          'thursday' => 0,
          'tuesday' => 0,
          'wednesday' => 0,
        ),
        'bdtpicker_enabled' => 1,
        'bdtpicker_side_by_side' => 0,
        'increment' => 3,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('End date is optional');
  t('End date is required');
  t('Single');

  return $field_instances;
}
