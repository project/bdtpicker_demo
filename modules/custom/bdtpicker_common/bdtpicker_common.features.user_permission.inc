<?php
/**
 * @file
 * bdtpicker_common.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bdtpicker_common_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create bdtpicker_tester content'.
  $permissions['create bdtpicker_tester content'] = array(
    'name' => 'create bdtpicker_tester content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any bdtpicker_tester content'.
  $permissions['delete any bdtpicker_tester content'] = array(
    'name' => 'delete any bdtpicker_tester content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own bdtpicker_tester content'.
  $permissions['delete own bdtpicker_tester content'] = array(
    'name' => 'delete own bdtpicker_tester content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any bdtpicker_tester content'.
  $permissions['edit any bdtpicker_tester content'] = array(
    'name' => 'edit any bdtpicker_tester content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own bdtpicker_tester content'.
  $permissions['edit own bdtpicker_tester content'] = array(
    'name' => 'edit own bdtpicker_tester content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
