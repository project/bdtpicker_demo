
api = 2
core = 7.x

defaults[projects][subdir] = "contrib"
defaults[projects][destination] = "profiles/bdtpicker_demo"

projects[bdtpicker_demo][type] = "profile"
projects[bdtpicker_demo][destination] = "profiles"
projects[bdtpicker_demo][subdir] = ""
projects[bdtpicker_demo][download][type] = "git"
projects[bdtpicker_demo][download][branch] = "7.x-3.x"

includes[] = project-core.make
includes[] = project-contrib.make
includes[] = project-libraries-verified.make
includes[] = project-libraries-other.make

translations[] = de
translations[] = es
translations[] = fr
translations[] = hu
