
api = 2
core = 7.x

includes[] = project-core.make

projects[bdtpicker_demo][type] = profile
projects[bdtpicker_demo][version] = 3.x-dev

includes[] = project-core.make
includes[] = project-contrib.make
includes[] = project-libraries-verified.make
includes[] = project-libraries-other.make

translations[] = de
translations[] = es
translations[] = fr
