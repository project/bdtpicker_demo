
libraries[bootstrap-datetimepicker][download][type] = "get"
libraries[bootstrap-datetimepicker][download][url] = "https://github.com/Eonasdan/bootstrap-datetimepicker/archive/v3.1.3.tar.gz"

libraries[font-awesome][download][type] = "get"
libraries[font-awesome][download][url] = "http://github.com/FortAwesome/Font-Awesome/archive/v4.1.0.zip"

libraries[moment][download][type] = "get"
libraries[moment][download][url] = "https://github.com/moment/moment/archive/2.8.3.tar.gz"
